import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import App from './components/app/app';

ReactDOM.render(<App />, document.getElementById('root'));
